FROM debian:stretch

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y
RUN apt-get install php7.0 apache2 gnupg wget git zip unzip -y

RUN apt-get update && apt-get install \
	mysql-server \
    git \
    curl \
	php7.0-mysql \
	php-mcrypt \
	php7.0-curl \
	php7.0-gd \	
	php7.0-xml \
    phpunit \
    php-mbstring \
    php-bcmath \
    php-intl \
    php-zip \
    php-soap -y --no-install-recommends

RUN mkdir /data

VOLUME /var/www
VOLUME /var/lib/mysql_data
RUN cd / && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

EXPOSE 80
COPY entrypoint.sh /entrypoint.sh
COPY files/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY files/50-server.cnf /etc/mysql/mariadb.conf.d/50-server.cnf
ENTRYPOINT /entrypoint.sh
